﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YourFantasyBro.Startup))]
namespace YourFantasyBro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
